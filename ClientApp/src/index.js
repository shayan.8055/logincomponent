import 'bootstrap/dist/css/bootstrap.css';
import './font/VazirFont.css';
import './css/bootstrap-rtl.min.css';
import 'font-awesome/css/font-awesome.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Login } from './components/Login'
const rootElement = document.getElementById('root');

ReactDOM.render(
    <Login />,
    rootElement);


